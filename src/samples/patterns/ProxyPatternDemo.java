package samples.patterns;

public class ProxyPatternDemo {

    public static void main(String[] args){
        CommandExecutor executor = new CommandExecutorProxy("andriy", "/tmp/wrong_pwd");
        try {
            executor.runCommand("ls -ltr");
            executor.runCommand(" rm -rf abc.pdf");
        } catch (Exception e) {
            System.out.println("Exception Message::"+e.getMessage());
        }

    }

}