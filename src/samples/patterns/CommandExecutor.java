package samples.patterns;

/**
 * Created by andriy on 3/17/17.
 */
public interface CommandExecutor {
    public void runCommand(String cmd) throws Exception;
}
