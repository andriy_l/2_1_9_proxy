package samples.horstman;

import java.lang.reflect.*;
import java.util.*;

/**
 * This program demonstrates the use of proxies.
 * we use proxies and invocation handlers to trace method calls
 * @version 1.00 2000-04-13
 * @author Cay Horstmann
 */
public class ProxyIntegerTest
{
   public static void main(String[] args)
   {
      Object[] elements = new Object[1000];

      // fill elements with proxies for the integers 1 ... 1000
      for (int i = 0; i < elements.length; i++)
      {

         Integer value = i + 1;
          // construct wrapper
         InvocationHandler handler = new TraceHandler(value);
          // construct proxy for one or more interfaces
         Object proxy = Proxy.newProxyInstance(null, new Class[] { Comparable.class } , handler);
         elements[i] = proxy;
      }


      // construct a random integer
      Integer key = new Random().nextInt(elements.length) + 1;
       System.out.printf("Random key = %d \n",key);

      // search for the key
      int result = Arrays.binarySearch(elements, key);

      // print match if found
      if (result >= 0) {
          System.out.println(elements[result]);
          System.out.println("we used class: " + elements[result].getClass().getName());
          System.out.println("and his parent class is: " + elements[result].getClass().getSuperclass().getName());
          System.out.println("is it a Proxy class?: " + Proxy.isProxyClass(elements[result].getClass()));
          System.out.println("is it a Proxy class?: " + Proxy.isProxyClass(new Integer(100).getClass()));
      }
   }
}

/**
 * We define a TraceHandler wrapper class that stores a wrapped object. Its invoke
 * method simply prints the name and parameters of the method to be called and
 * then calls the method with the wrapped object as the implicit parameter.
 *
 * An invocation handler that prints out the method name and parameters, then
 * invokes the original method
 */
class TraceHandler implements InvocationHandler
{
   private Object target;

   /**
    * Constructs a TraceHandler
    * @param t the implicit parameter of the method call
    */
   public TraceHandler(Object t)
   {
      target = t;
   }

   // щоразу при виклику методу вказаного інтерфейсу виводиться ім'я проксі-об'єкту і параметри
   public Object invoke(Object proxy, Method m, Object[] args) throws Throwable
   {
      // print implicit argument
      System.out.print(target);
      // print method name
      System.out.print("." + m.getName() + "(");
      // print explicit arguments
      if (args != null)
      {
         for (int i = 0; i < args.length; i++)
         {
            System.out.print(args[i]);
            if (i < args.length - 1) System.out.print(", ");
         }
      }
      System.out.println(")");

      // invoke actual method
      return m.invoke(target, args);
   }
}
